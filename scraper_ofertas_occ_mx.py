# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.utils import timezone

now = timezone.utcnow()
a_date = timezone.datetime(2020, 10, 9)

default_args = {
    'owner': 'analytics talentu',
    'depends_on_past': False,
    'start_date': datetime(2020, 1, 1),
}

with DAG(dag_id='AA_get_cvs_general_robots',
         default_args=default_args,
         schedule_interval='@daily',
         catchup=False,
         max_active_runs=1,
         concurrency=1
         ) as dag:

    run_get_cvs_talentu = BashOperator(task_id="run_get_cvs_talentu_from_bash",
                                            bash_command="/var/www/html/col_sources/scraper_ct_col/run_get_cvs_talentu.bash ")
       
# always add final " " at bash_command
run_get_cvs_talentu