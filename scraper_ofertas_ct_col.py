# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.utils import timezone
import pendulum


default_args = {
    'owner': 'analytics talentu',
    'depends_on_past': False,
    'start_date': pendulum.datetime(year=2020, month=7, day=17).astimezone('America/Bogota'),
}


with DAG('AA_close_talentu',
         default_args=default_args,
         schedule_interval='@daily',
         catchup = False,
         concurrency=1
         )as dag:
    run_close_talentu = BashOperator(task_id="run_test_time_zone_3",
                                      bash_command="/var/www/html/col_sources/scraper_ct_col/run_close_talentu.bash ")
# always add final " " at bash_command
run_close_talentu

