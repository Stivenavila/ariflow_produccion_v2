from datetime import datetime, timedelta
from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator


dag_id='clean_scheduler_logs'


default_args = {
    'owner': 'analytics talentu',
    'start_date': datetime(2022, 3, 7),
    'provide_context': True
}

with DAG(dag_id,
        default_args=default_args,
        schedule_interval='@daily',
        catchup=False,
        max_active_runs=1,
        concurrency=1
        ) as dag:

    clean_scheduler_logs = BashOperator(task_id='clean_scheduler_logs',
                            bash_command="sudo find /root/airflow/logs/* -mtime +1 -exec rm -r {} \;")
       
# always add final " " at bash_command
clean_scheduler_logs